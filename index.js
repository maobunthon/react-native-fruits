import { AppRegistry } from 'react-native';
import FruitList from './src/components/FruitList';
import {
  StackNavigator,
} from 'react-navigation';
import FruitDetail from './src/components/FruitDetail';

const Navigation = StackNavigator({
	Home: {screen: FruitList},
	Detail: {screen: FruitDetail}
});

AppRegistry.registerComponent('fruits', () => Navigation);
