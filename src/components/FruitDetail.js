import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  TouchableOpacity,
} from 'react-native';

export default class FruitDetail extends Component {
	static navigationOptions = ({ navigation }) => ({
    	title: `${navigation.state.params.fruit.name}`,
    });

    state = { 
    	description: this.props.navigation.state.params.fruit.description,
    	numberOfLine: 3, 
    	showbutton: 'SHOW MORE',
    	colorOrange: true
    };

    showLess = () => {
    	let showbutton = 'SHOW MORE';
    	let numOfLine = 3;
    	if (this.state.showbutton === 'SHOW MORE') {
    		numOfLine = 20;
    		showbutton = 'SHOW LESS';
    	}

    	this.setState({
    		description: this.props.navigation.state.params.fruit.description,
    		numberOfLine: numOfLine,
    		showbutton: showbutton,
    		colorOrange: !this.state.colorOrange
    	})
    }

	render() {
		let image = './../../images/' + this.props.navigation.state.params.fruit.image;
		let color = this.state.colorOrange ? '#6bb1ff' : '#f5a623';
		const { navigate } = this.props.navigation;
		
	    return (
	      <View style={styles.container}>
	      	<View style={styles.row}>
		      	<Text style={styles.title}>{this.props.navigation.state.params.fruit.name}</Text>
		      	<TouchableOpacity activeOpacity = { .5 } onPress={() =>navigate("Home")}>
		          <Image source={require('./../../images/cross-white.png')} />          
		        </TouchableOpacity>
	        </View>
	        <Image source={require('./../../images/apple.png')} />
	        <View style={styles.row}>
		      	<Text style={styles.description} renderTruncatedFooter={() => <ReadMoreButton />} numberOfLines={this.state.numberOfLine}>{this.state.description}</Text>
	        </View>
	        <View style={styles.showBtn}>
		      	<TouchableOpacity style={{backgroundColor: color, padding: 10}} onPress={this.showLess} >
	              <Text style={styles.btnText}>{this.state.showbutton}</Text>
	            </TouchableOpacity>
	        </View>
	      </View>
	    );
	}


}

const styles = StyleSheet.create({
	container: {
	    backgroundColor: '#3d75b2',
	    flex: 1,
	    justifyContent: 'flex-start',
	    alignItems: 'center',
	},
	row: {
	    width: '80%',
	    justifyContent: 'space-between',
	    flexDirection: 'row',
	    margin: 30,
	},
	title: {
	  	color: '#ffffff',
	  	fontSize: 24
	},
	description: {
	  	color: '#ffffff',
	  	fontSize: 16
  	},
  	showBtn: {
  		flex: 1,
  		alignSelf: 'flex-end',
  		marginRight: '10%'
  	},
  	btn: {
  		backgroundColor: 'orange',
	    alignSelf: 'flex-end',
	    padding: 10,
	    margin: 10
  	},
  	btnText: {
  		color: '#ffffff'
  	}
});	