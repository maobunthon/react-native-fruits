import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  TouchableOpacity,
} from 'react-native';
import Logo from './Logo';
import Fruits from './../../data/Fruits.json';

export default class FruitList extends Component {
  static navigationOptions = {
    title: 'List Page',
    headerLeft: null
  };

  state = { data: [] };

  componentWillMount() {
    this.setState({ data: Fruits, temp: Fruits });
  }

  goDetail = (fruit) => {
    Alert.alert(fruit.name);
  }

  shuffleArray(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  onShuffle = () => {
    this.setState({data: this.shuffleArray(this.state.data)});
  }

  onReverse = () => {
    let reversed = this.state.data.reverse();
    this.setState({data: reversed});
  }

  onDeleteItem = (fruit) => {
    Alert.alert(
      'Are you sure you want to delete ' + fruit.name + '?',
      'Click OK to continue, Cancel to abort this action.',
      [
        {text: 'Cancel', style: 'cancel'},
        {
          text: 'OK', onPress: () => 
          this.setState({
            data: this.state.data.filter(function(item) { 
              return item.id !== fruit.id
            })
          })
        },
      ],
      { cancelable: false }
    )
  }

  onFilter = () => {
    let result = [];
    this.state.data.map(function (fruit){
      if (fruit.name.startsWith('B')) {
        return result.push(fruit);
      }
    });
    this.setState({data: result});
  }

  fruitItem() {
    const { navigate } = this.props.navigation;
    return this.state.data.map(fruit => 
      <View style={styles.item} key={fruit.id} >
        <Text onPress={() =>navigate("Detail",{fruit: fruit})}>{fruit.name}</Text>
        <TouchableOpacity activeOpacity = { .5 } onPress={this.onDeleteItem.bind(this, fruit)} >
          <Image source={require('./../../images/cross-black.png')} />          
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}><Logo /></View>
        {this.fruitItem()}
        <View style={styles.row}>
          <Text style={styles.total}>TOTAL: {this.state.data.length}</Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.btn} onPress={this.onShuffle} >
              <Text style={styles.btnText}> SHUFFLE </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.btn} onPress={this.onReverse} >
              <Text style={styles.btnText}> REVERSE </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.btn} onPress={this.onFilter} >
              <Text style={styles.btnText}> FILTER </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3d75b2',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  logo: {
    margin: 10,
    paddingTop: 30,
  },
  item: {
    backgroundColor: '#ffffff',
    padding: 10,
    width: '80%',
    margin: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  total: {
    color: '#ffffff',
    paddingLeft: '10%',
  },
  row: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  buttonContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    margin: 10,
  },
  btn: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#6bb1ff',
    padding: 10,
    margin: 10
  },
  btnText: {
    color: '#ffffff'
  }
});
