import React from 'react';
import { AppRegistry, Text, Image } from 'react-native';

const Logo = () => (
    <Image source={require('./../../images/logo.png')} />
);

export default Logo;